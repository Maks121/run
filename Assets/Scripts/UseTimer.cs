using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UseTimer : MonoBehaviour
{
    [SerializeField] private float timer;
    [SerializeField] private float autoTimer;

    private bool active, autoActive;

    //private float timer;

    public float Timer { get => timer; set => timer = value; }
    public float AutoTimer { get => autoTimer; set => autoTimer = value; }

    public void InitTimer(float currentTime)
    {
        
        StartCoroutine(StartTimer(currentTime));
        
    }
    public void InitAutoTimer(float currentTime)
    {
        StartCoroutine(StartAutoTimer(currentTime));
    }

   
    private IEnumerator<WaitForSeconds> StartTimer(float countdownValue )
    {
        timer = countdownValue;
        while (timer > 0)
        {
            yield return new WaitForSeconds(1.0f);
            timer--;
        }
    }
    private IEnumerator<WaitForSeconds> StartAutoTimer(float countdownValue)
    {
        autoTimer = countdownValue;
        while(true)
        {
            if(autoTimer >= 0) {
                Debug.Log("Countdown: " + autoTimer);
                yield return new WaitForSeconds(1.0f);
                autoTimer--;
            }
            else
            {
                Debug.Log("repeat");
                autoTimer = countdownValue;
            }
        }
    }
}
