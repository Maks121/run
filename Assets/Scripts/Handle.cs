using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Handle : MonoBehaviour, IBeginDragHandler, IDragHandler
{
    [SerializeField] private Player player;
    [SerializeField] private AudioSource SoundRun;




    public void OnBeginDrag(PointerEventData eventData)
    {
            if (eventData.delta.x < 0 && player.transform.position.x > player.minHeight && !player.GameOver)
            {
                player.targetPos = new Vector2(player.minHeight, player.transform.position.y);
                if (player.takeD == false)
                {
                    player.anim.SetTrigger("run");
                    SoundRun.Play();
                } 
               
        }
            else if (eventData.delta.x > 0 && player.transform.position.x < player.maxHeight && !player.GameOver)
            {
               
                player.targetPos = new Vector2(player.maxHeight, player.transform.position.y);
            if (player.takeD == false)
            {
                player.anim.SetTrigger("run");
                SoundRun.Play();
            }
        }

    }

    public void OnDrag(PointerEventData eventData)
    {
       
    }

   
    
}
