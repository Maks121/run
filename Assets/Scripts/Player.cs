﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    public ParticleSystem[] particale;
    public GameObject PositionPlayer;

    public Animator anim;
    public Animation[] anim1;
    private Spawner spawner;
    public Vector2 targetPos;
  
    public bool GameOver;

    [SerializeField] private Save save;
    [SerializeField] private UiInGame UI;
    [SerializeField] private AudioSource SoundRun;
    [SerializeField] private AudioSource SoundDamage;
    [SerializeField] private AudioSource SoundTake;
    [SerializeField] private Tutorial tutorial;

    public float speed;
    public float maxHeight;
    public float minHeight;

    public bool takeD;

    private void Start()
    {
        anim = GetComponentInChildren<Animator>();
        spawner = FindObjectOfType<Spawner>();        
        targetPos = PositionPlayer.transform.position;
        
    }
    // Update is called once per frame
  
    private void Update()
    {
      
        PositionPlayer.transform.position = Vector2.MoveTowards(transform.position, targetPos, 5f * Time.deltaTime);

        if (UI.Health <= 0 && !GameOver)
            {
                UI.panelGameOver.SetActive(true);
                GameOver = true;
                Time.timeScale = 0;
            save.SG();

            }
 
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!GameOver) { 
        var collider = GetComponent<Collider2D>();
        if (other.CompareTag("addDamage"))
        {
            takeD = true;
            UI.Health--;
            anim.SetTrigger("damage");
            SoundDamage.Play();
            collider.enabled = false;
            StartCoroutine(AfterMove(collider));
            Destroy(other.gameObject);
            Instantiate(particale[0], other.transform.position, Quaternion.identity);
                
        }
        if (other.CompareTag("addDamageBox"))
        {
                takeD = true;
                UI.Health--;
                anim.SetTrigger("damage");
                SoundDamage.Play();
                collider.enabled = false;
                StartCoroutine(AfterMove(collider));
                Destroy(other.gameObject);
                Instantiate(particale[1], other.transform.position, Quaternion.identity);
                
        }
            if (other.CompareTag("addHealth"))
        {
                UI.Health++;
                anim1[0].Play();
                SoundTake.Play();
                Destroy(other.gameObject);
        }
            if (other.CompareTag("wave"))
            {
                DataBase.Instance.Wave++;
                SoundTake.Play();
                Destroy(other.gameObject);
            }
            if (other.CompareTag("slowdown"))
            {
                DataBase.Instance.SlowDown++;
                SoundTake.Play();
                Destroy(other.gameObject);
            }
            if (other.CompareTag("shield"))
            {
                DataBase.Instance.Shield++;
                SoundTake.Play();
                Destroy(other.gameObject);
            }
            
        if (other.CompareTag("addMoney"))
            {
                DataBase.Instance.Money++;
                SoundTake.Play();
                anim1[1].Play();
                Destroy(other.gameObject);
            }
        }
        if (other.CompareTag("star"))
        {
            UI.Star++;
            SoundTake.Play();
            Destroy(other.gameObject);
        }
        if (other.CompareTag("tut"))
        {
            tutorial.Trigger();
        }
    }
    private void Save()
    {
        PlayerPrefs.SetInt("score", UI.Score);
        PlayerPrefs.SetInt("health", UI.Health);
        PlayerPrefs.Save();
    }
  
    private IEnumerator AfterMove(Collider2D other)
    {
        
        yield return new WaitForSeconds(1f);
        other.enabled = true;
        takeD = false;
        
    }   
}
