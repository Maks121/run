using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    [SerializeField] private Save save;

    [SerializeField] private Text moneyText;
    [SerializeField] private Text WaveText;
    [SerializeField] private Text ShieldText;
    [SerializeField] private Text SlowDownText;
    [SerializeField] private GameObject buttonReward;

    private const string AdMobRewards = "ca-app-pub-8934470786012160/5168441638";
    private RewardedAd RewAd;

    private int priceWave = 30;
    private int priceShield = 25;
    private int priceSlowDown = 20;

    private void Update()
    {
        moneyText.text = DataBase.Instance.Money.ToString();
        WaveText.text = DataBase.Instance.Wave.ToString();
        ShieldText.text = DataBase.Instance.Shield.ToString();
        SlowDownText.text = DataBase.Instance.SlowDown.ToString();
    }
    public void WaveAdd()
    {
        if (DataBase.Instance.Money >= priceWave)
        {
            DataBase.Instance.Wave++;
            DataBase.Instance.Money -= priceWave;
            save.SG();
        }
        
    }
    public void ShieldAdd()
    {
        if (DataBase.Instance.Money >= priceShield)
        {
            DataBase.Instance.Shield++;
            DataBase.Instance.Money -= priceShield;
            save.SG();
        }
        
    }
    public void SlowDownAdd()
    {
        if (DataBase.Instance.Money >= priceSlowDown)
        {
            DataBase.Instance.SlowDown++;
            DataBase.Instance.Money -= priceSlowDown;
            save.SG();
        }
        
    }
    private void OnEnable()
    {
        RewAd = new RewardedAd(AdMobRewards);
        AdRequest request = new AdRequest.Builder().Build();
        RewAd.LoadAd(request);

        RewAd.OnUserEarnedReward += HandleUserEarnedReward;

    }
    private void OnDisable()
    {
        RewAd.OnUserEarnedReward -= HandleUserEarnedReward;
    }
    public void adMobRewards()
    {
        if (RewAd.IsLoaded()) RewAd.Show();
        buttonReward.SetActive(false);
    }
    public void HandleUserEarnedReward(object sender, Reward args)
    {
        DataBase.Instance.Money += 30;
        save.SG();
    }
}
