using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
   
    public GameObject panelShop;
    public GameObject panelLvl;
    public GameObject onDestroy;
    [SerializeField] private AdMob adMob;
    [SerializeField] private Save save;
    [SerializeField] private EditLocation editLocation;

    public Sprite[] sprites;
    public GameObject soundButton;
    [SerializeField] private AudioSource AudioSourceButton;


    public RawImage image; 

    public Animation anim;
    private void Start()
    {
        save.LG();
        SoundIcon();

        if (File.Exists(Application.persistentDataPath + "/Screen.png"))
        {
            string path = Application.persistentDataPath + "/Screen.png";
            byte[] data = File.ReadAllBytes(path);
            Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
            screenImage.LoadImage(data);
            image.texture = screenImage;
        }
    }
    private void SoundIcon()
    {
        if (DataBase.Instance.SoundOn == true)
        {
            AudioListener.volume = 1;
            soundButton.transform.GetComponent<Image>().sprite = sprites[0];

        }
        else
        {
            AudioListener.volume = 0;
            soundButton.transform.GetComponent<Image>().sprite = sprites[1];

        }
    }
    public void UseScrenshot()
    {
        if (File.Exists(Application.persistentDataPath + "/Screen.png"))
        {
            string path = Application.persistentDataPath + "/Screen.png";
            byte[] data = File.ReadAllBytes(path);
            Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
            screenImage.LoadImage(data);
            image.texture = screenImage;
            //Debug.Log("screen upload");
        }
    }
    IEnumerator captureScreenshot()
    {
        yield return new WaitForEndOfFrame();
        //about to save an image capture
        Texture2D screenImage = new Texture2D(Screen.width, Screen.height);
        string path = Application.persistentDataPath + "/Screen.png";
       
        //Get Image from screen
        screenImage.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenImage.Apply();

        
        //Convert to png
        byte[] imageBytes = screenImage.EncodeToPNG();

        //Save image to file
        File.WriteAllBytes(path, imageBytes);
        
    }
    public void NewGame()
    {
        AudioSourceButton.Play();
        DontDestroyOnLoad(onDestroy);
        adMob.BannerDestroy();
        SceneManager.LoadScene("1lvl");
        editLocation.ColorEditGame();
    }
    
    public void SoundOff()
    {
        AudioSourceButton.Play();
        if (DataBase.Instance.SoundOn == true)
        {
            AudioListener.volume = 0;
            soundButton.transform.GetComponent<Image>().sprite = sprites[1];
            DataBase.Instance.SoundOn = false;
            save.SG();
        }
        else 
        {
            AudioListener.volume = 1;
            soundButton.transform.GetComponent<Image>().sprite = sprites[0];
            DataBase.Instance.SoundOn = true;
            save.SG();
        } 
    }
    public void LVlActive()
    {
        AudioSourceButton.Play();
        panelLvl.SetActive(!panelLvl.activeSelf);
        adMob.BannerDestroy();
        StartCoroutine(captureScreenshot());

    }
    public void LVlClose()
    {
        AudioSourceButton.Play();
        UseScrenshot();
        adMob.Banner();
        panelLvl.SetActive(!panelLvl.activeSelf);
               
    }
    public void Shop()
    {
        AudioSourceButton.Play();
        panelShop.SetActive(!panelShop.activeSelf);
    }
    public void TutorialBttn()
    {
        AudioSourceButton.Play();
        SceneManager.LoadScene("tutorial");
    }


    public void Exit()
    {
        AudioSourceButton.Play();
        Application.Quit();
    }
}
