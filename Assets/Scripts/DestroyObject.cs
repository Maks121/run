using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyObject : MonoBehaviour
{
    public float timeDestroy;
    void Update()
    {
        Destroy(gameObject, timeDestroy);
        
    }
}
