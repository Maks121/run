using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Tutorial : MonoBehaviour
{
    [SerializeField] Spawner spawner;
   

    public GameObject[] tutorialPan;
   
   
    private int panelActive;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Close()
    {
        tutorialPan[panelActive].SetActive(!tutorialPan[panelActive].activeSelf);
        spawner.speed = 2.5f;
        panelActive++;
    }
    public void Trigger()
    {
        tutorialPan[panelActive].SetActive(!tutorialPan[panelActive].activeSelf);
        spawner.speed = default;

    }
    
}
