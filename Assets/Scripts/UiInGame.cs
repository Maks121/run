using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiInGame : MonoBehaviour
{
    public Text moneyText;
    [SerializeField] private LevelManager levelManager;


    public GameObject panelGameOver;
    public GameObject panelPause;
    public GameObject panelPauseLvl;
    public GameObject panelComplete;
    public GameObject ButtonGameOverReward;

    public Text healthLabel;
    public Text scoreLabel;
    public Text starLabel;

    public Text waveTxt;
    public Text shieldTxt;
    public Text slowDownTxt;
    public CircleCollider2D playerCollider;
    [SerializeField] private AudioSource AudioSourceButton;


    [SerializeField] private Save save;
    [SerializeField] private Player player;
    
    [SerializeField] private AdMob adMob;

    [SerializeField] private int score;
    [SerializeField] private int health;
    [SerializeField] private int star;


    public int Star
    {
        get
        {
            return star;
        }
        set
        {
            star = value;
            if (starLabel != null)
            {
                starLabel.text = star + " x";
            }

        }
    }

    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
            scoreLabel.text = "Score:" + score;
        }
    }
    public int Health
    {
        get
        {
            return health;
        }
        set
        {

            health = value;
            healthLabel.text = health + " x";
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.HasKey("score")) Load();
        else Init();
        //adMob.Banner();
        //adMob.ActiveAdMob(0);
    }
    private void Init()
    {

        if (SceneManager.GetActiveScene().buildIndex == 3)
        {
            Health = 50;
        }
        else Health = DataBase.Instance.HealthGlobal;
        levelManager = FindObjectOfType<LevelManager>();
        adMob = FindObjectOfType<AdMob>();
        Time.timeScale = 1;
        save.LG();
    }

    private void Load()
    {
        Score = PlayerPrefs.GetInt("score", Score);
        Health = PlayerPrefs.GetInt("health", Health);
    }

    // Update is called once per frame
    void Update()
    {
        moneyText.text = DataBase.Instance.Money.ToString();
        waveTxt.text = DataBase.Instance.Wave.ToString();
        shieldTxt.text = DataBase.Instance.Shield.ToString();
        slowDownTxt.text = DataBase.Instance.SlowDown.ToString();
    }
    public void NewGame()
    {
        AudioSourceButton.Play();
        SceneManager.LoadScene("1lvl");
        player.GameOver = false;
        panelGameOver.SetActive(false);
        PlayerPrefs.DeleteAll();
        save.SG();

    }
    public void NextLvl()
    {
        AudioSourceButton.Play();
        levelManager.currentIndex += 1;
        PlayerPrefs.DeleteAll();
        save.SG();
        levelManager.RunLevel(levelManager.currentIndex);
    }
    public void NewGameLvl()
    {
        AudioSourceButton.Play();
        SceneManager.LoadScene("3lvl");
        Time.timeScale = 1;
        player.GameOver = false;
        PlayerPrefs.DeleteAll();
        save.SG();
        

    }
    public void PauseLvl()
    {
        AudioSourceButton.Play();
        Time.timeScale = 0;
        panelPauseLvl.SetActive(true);
        player.GameOver = true;
    }
    public void RunGameLvl()
    {
        AudioSourceButton.Play();
        Time.timeScale = 1;
        panelPauseLvl.SetActive(false);
        player.GameOver = false;
    }
    public void MainMenu()
    {
        AudioSourceButton.Play();
        PlayerPrefs.DeleteAll();
        save.SG();
        Time.timeScale = 1;
        Destroy(GameObject.FindGameObjectWithTag("OnDestroy"));
        SceneManager.LoadScene("menu");
    }
    public void Pause()
    {
        AudioSourceButton.Play();
        panelPause.SetActive(true);
        player.GameOver = true;
        Time.timeScale = 0;
    }
    public void Run()
    {
        AudioSourceButton.Play();
        panelPause.SetActive(false);
        player.GameOver = false;
        Time.timeScale = 1;
    }
    public void AddHealthReward()
    {
        Health += 2;
        playerCollider.enabled = false;
        StartCoroutine(ActiveCol());
        panelGameOver.SetActive(false);
        player.GameOver = false;
        Time.timeScale = 1;
    }
    
    private IEnumerator ActiveCol()
    {

        yield return new WaitForSeconds(1f);
        playerCollider.enabled = true;
    }
}

