using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;

public class AdMob : MonoBehaviour
{
    public BannerView bannerV;
    
    private const string banner = "ca-app-pub-8934470786012160/5859169792";
    private const string adMobGameOver = "ca-app-pub-8934470786012160/5955566238";
   

    private int GameLose;
    // Start is called before the first frame update
    void Start()
    {
        Configuration();
    }
    public void Banner()
    {
        StartCoroutine(BannnerTime());
    }
    private IEnumerator BannnerTime()
    {
        yield return new WaitForSeconds(.1f);
        this.bannerV = new BannerView(banner, AdSize.Banner, AdPosition.Bottom);
        AdRequest request = new AdRequest.Builder().Build();
        this.bannerV.LoadAd(request);
    }
    public void BannerDestroy()
    {
        this.bannerV.Destroy();
    }
    private void AdMobGameOver()
    {
        
        InterstitialAd ad = new InterstitialAd(adMobGameOver);
        AdRequest request = new AdRequest.Builder().Build();
        ad.LoadAd(request);
        if (ad.IsLoaded()) ad.Show();
    } 
    
    public void Configuration()
    {
        List<string> deviceIds = new List<string>();
        deviceIds.Add("2A5971650A6DF4D6370158357E190D8D");
        RequestConfiguration requestConfiguration = new RequestConfiguration
    .Builder()
    .SetTestDeviceIds(deviceIds)
    .build();
        MobileAds.SetRequestConfiguration(requestConfiguration);
    }

    public void ActiveAdMob(int countLose)
    {
        
        if (GameLose >= countLose)
        {
            AdMobGameOver();
            GameLose = default;
        }
        else GameLose++;
    }

}
