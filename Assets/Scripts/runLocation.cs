﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class runLocation : MonoBehaviour
{


    public float speed;
    private Spawner spawner;
    private bool spawned;
    private Player player;
    private UiInGame UI;

    



    // Start is called before the first frame update
    private void Start()
    {
        player = FindObjectOfType<Player>();
        UI = FindObjectOfType<UiInGame>();
        spawner = FindObjectOfType<Spawner>();
        
        
       
    }

    // Update is called once per frame
    void Update()
    {
        speed = spawner.speed;
        if (player.GameOver == false)
            {
                transform.Translate(Vector2.down * speed * Time.deltaTime);
                speed += spawner.speedIncrease * Time.deltaTime;
                if (transform.position.y < 0 && !spawned)
                {
                    spawner.SpawnWave();
                    spawned = true;
                }
                if (transform.position.y < -10)
                {
                   UI.Score += 2;
                   Destroy(gameObject);
                   spawned = false;
               
                }
            }
        
        


    }
    
    
}
