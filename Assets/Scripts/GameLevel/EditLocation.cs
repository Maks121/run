using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EditLocation : MonoBehaviour
{
    private GameObject[] prefabEdit;
    public GameObject[] locationEditGame;
    public Sprite[] randomSprite;
    public ParticleSystem PS;
    
    public void ColorEdit(Sprite spritesBlocks , List<GameObject> Location)
    {
        
        prefabEdit = new GameObject[Location.Count];
        
        for (int i = 0; i < Location.Count; i++)
        {
            prefabEdit[i] = Location[i];
            
            for (int a = 0; a < prefabEdit[i].transform.GetChild(0).transform.childCount ; a++)
            {
                prefabEdit[i].transform.GetChild(0).GetChild(a).GetComponent<SpriteRenderer>().sprite = spritesBlocks;
            }
        }
          
            
        PS.textureSheetAnimation.SetSprite(0, spritesBlocks);

    }
    public void ColorEditGame()
    {
        int rand = Random.Range(0, randomSprite.Length);
        for (int i = 0; i < locationEditGame.Length; i++)
        {
            for (int a = 0; a < locationEditGame[i].transform.GetChild(0).transform.childCount; a++)
            {
                
                    locationEditGame[i].transform.GetChild(0).GetChild(a).GetComponent<SpriteRenderer>().sprite = randomSprite[rand];
                 
            }
        }

        PS.textureSheetAnimation.SetSprite(0, randomSprite[rand]);
    }
}
