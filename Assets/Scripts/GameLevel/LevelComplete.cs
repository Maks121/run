using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelComplete : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private UiInGame UI;
    [SerializeField] private Save save;

    public Image starImage;
    public GameObject starPanel;
    public int cur;

    
    void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
        save = FindObjectOfType<Save>();
        cur = levelManager.currentIndex;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("finish"))
        {
            player.GameOver = true;
            UI.panelComplete.SetActive(true);
            if(levelManager.currentIndex == DataBase.Instance.CountLevel)
            {
                DataBase.Instance.CountLevel++;
                save.SG();
            }
            takeStar();
            Time.timeScale = 0;
        }
        
    }
    private void takeStar()
    {
       
        for (int i = 0; i < UI.Star; i++)
        {
            Instantiate(starImage, starPanel.transform);
        } 

        var lvlstar = DataBase.Instance.LvlStar;      
        var count = DataBase.Instance.LvlStar[cur];

        if (UI.Star >= count)
            {
               
                lvlstar[cur] = UI.Star;
                
        }
    }
}
