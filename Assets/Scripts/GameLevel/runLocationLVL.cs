﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class runLocationLVL : MonoBehaviour
{


    public float speed;
    private Spawner spawner;
    private UiInGame UI;
    private bool spawned;
    
    private Player player;

    



    // Start is called before the first frame update
    private void Start()
    {
        player = FindObjectOfType<Player>();
        spawner = FindObjectOfType<Spawner>();
        UI = FindObjectOfType<UiInGame>();
        
       
    }

    // Update is called once per frame
    void Update()
    {
        speed = spawner.speed;
        if (player.GameOver == false)
            {
                transform.Translate(Vector2.down * speed * Time.deltaTime);
                speed += spawner.speedIncrease * Time.deltaTime;
                
                if (transform.position.y < 0 && !spawned)
                {
                    spawner.SpawnWaveLvl();
                    spawned = true;
                }
                if (transform.position.y < -10)
                {
                   UI.Score += 2;
                   Destroy(gameObject);
                   spawned = false;
               
                }
            }

    }
   
}
