using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using GoogleMobileAds.Api;


public class LevelManager : MonoBehaviour
{
   
    [SerializeField] private Save save;
    [SerializeField] private AdMob adMob;
    [SerializeField] private EditLocation editLocation;

    public GameObject onDestroy;
    public Level[] Levels;
    public Level curLvl;
    public List<object> ListLevels;
    public GameObject panelButton;
    private Sprite CurrentSprite;
    public Sprite[] sprites;
    public Image starImage;
    public GameObject tutorial;
    

  
    public int currentIndex;
    public bool activLvl = false;
    // Start is called before the first frame update
    public void CountButton(int index)
    {
        
        currentIndex = index;
        RunLevel(index);
        activLvl = true;
    }
    private void Start()
    {
        MobileAds.Initialize(initStatus => { });
        save.LG();
        if (DataBase.Instance.LvlStar.Length != 0)
        {
            adMob.Banner();
        }

            ListLevels = new List<object>(Levels);

        for (int i = 0; i < panelButton.transform.childCount; i++)
        {
            panelButton.transform.GetChild(i).transform.GetChild(0).GetComponent<Text>().text = (i + 1).ToString();

            if (i <= DataBase.Instance.CountLevel)
            {
                panelButton.transform.GetChild(i).GetComponent<Button>().interactable = true;
                panelButton.transform.GetChild(i).GetComponent<Image>().sprite = sprites[0];

                if (DataBase.Instance.LvlStar.Length != 0)
                {
                        
                        var transform = panelButton.transform.GetChild(i).GetChild(1);
                        for (int a = 0; a < DataBase.Instance.LvlStar[i]; a++)
                        {
                            Instantiate(starImage, transform.transform);
                        }
                }
                else
                {
                    DataBase.Instance.LvlStar = new int[50];
                    DataBase.Instance.SoundOn = true;
                    tutorial.SetActive(true);
                    save.SG();
                }
            }
            else
            {
                panelButton.transform.GetChild(i).GetComponent<Button>().interactable = false;
                panelButton.transform.GetChild(i).GetComponent<Image>().sprite = sprites[1];
            }

        }
   
    }
   
    public void TutorialBttn()
    {
        tutorial.SetActive(false);
    }
 
    public void RunLevel(int indexBttn)
    {
        var sc = SceneManager.GetActiveScene().buildIndex;
        if (sc == 0)
        {
            panelButton.transform.GetChild(indexBttn).GetComponent<Image>().sprite = sprites[2];
        }
        //�������� ������ � ������� � ��������� �����
        curLvl = ListLevels[indexBttn] as Level;
        //CurrentSprite = curLvl.currentSprite;
        List<GameObject> lvl = new List<GameObject>(curLvl.Location);
        editLocation.ColorEdit(curLvl.currentSprite, lvl );

        DontDestroyOnLoad(onDestroy);
        
        SceneManager.LoadScene("3lvl");
    }
}
[Serializable]
public class Level
{
    public GameObject[] Location;
    public Sprite currentSprite;
}