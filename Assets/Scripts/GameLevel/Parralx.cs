using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parralx : MonoBehaviour
{
    public float speed;

    public float startY;
    public float endY;

    private void Update()
    {
        transform.Translate(Vector3.down * speed * Time.deltaTime);

        if (transform.position.y <= endY )
        {
            Vector3 pos = new Vector3(transform.position.x, startY, transform.position.z);
            transform.position = pos;
        }
    }
}
