using UnityEngine;
using GoogleMobileAds.Api;


public class AdMobRew : MonoBehaviour
{
    private const string AdMobRewards = "ca-app-pub-8934470786012160/5168441638";
    private RewardedAd RewAd;
    [SerializeField] UiInGame U;

      private void OnEnable()
    {
        RewAd = new RewardedAd(AdMobRewards);
        AdRequest request = new AdRequest.Builder().Build();
        RewAd.LoadAd(request);

        RewAd.OnUserEarnedReward += HandleUserEarnedReward;
        
    }
    private void OnDisable()
    {
        RewAd.OnUserEarnedReward -= HandleUserEarnedReward;
        
    }
    public void adMobRewards()
    {

        if (RewAd.IsLoaded()) RewAd.Show();
        U.ButtonGameOverReward.SetActive(false);
    }
    public void HandleUserEarnedReward(object sender, Reward args)
    {
        U.AddHealthReward();
    }
    
}
