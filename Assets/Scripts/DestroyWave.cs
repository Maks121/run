using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWave : MonoBehaviour
{
    public ParticleSystem[] particle;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("addDamageBox"))
        {
            
            Destroy(other.gameObject);
            Instantiate(particle[0], other.transform.position, Quaternion.identity);
            
        }
        if (other.CompareTag("addDamage"))
        {
            Destroy(other.gameObject);
            Instantiate(particle[1], other.transform.position, Quaternion.identity);
            
        }
    }
}
