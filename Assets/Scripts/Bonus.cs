using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bonus : MonoBehaviour
{
    [SerializeField] private UseTimer T;
    [SerializeField] private UseTimer Tslow;
    [SerializeField] private Spawner spawner;
  
    private float currentSpeed;
    public Button[] bttnBonus;
    public GameObject player;
    public GameObject shieldPosition;
    public GameObject SlowDownPosition;
    public Image imageShield;
    public Image imageSlowDown;
   
    public GameObject[] prefab;
   
    private Vector2 target;
    private bool activeShield;
    private bool activeSlowDown;
    private bool activeCurrent = false;

    public void OnButton(int index)
    {
        
       
        switch (index)
        {
            case 0:
                Shield();
                break;
            case 1:
                WaveDestroy();
                break;
            case 2:
                SlowDown();
                break;
        }
                
    } 
   
    private void WaveDestroy()
    {
        if(DataBase.Instance.Wave >= 1)
        {
            DataBase.Instance.Wave--;
            Instantiate(prefab[1], target, Quaternion.identity);
        }
        
    }
    private void Shield()
    {
        if (!activeShield && DataBase.Instance.Shield >= 1)
        {
            DataBase.Instance.Shield--;
            Instantiate(prefab[0], shieldPosition.transform);
           
            T.InitTimer(5f);
            activeShield = true;
            
        }
        
    }
    private void SlowDown()
    {
        if (!activeSlowDown && DataBase.Instance.SlowDown >= 1)
        {
            currentSpeed = spawner.speed;
            DataBase.Instance.SlowDown--;
            Tslow.InitTimer(5f);
            activeSlowDown = true;
            activeCurrent = true;
            Instantiate(prefab[2], SlowDownPosition.transform);
        }
       
    }
    // Start is called before the first frame update
  

    // Update is called once per frame
    void Update()
    {
       
        target = player.transform.position;
        if (T.Timer <= 0)
            {
                bttnBonus[0].interactable = true;
                activeShield = false;
                imageShield.fillAmount = 0;
            }
       
        else
            {
                bttnBonus[0].interactable = false;
                imageShield.fillAmount += 1f/(T.Timer+2f) * Time.deltaTime;
            }
        if (Tslow.Timer <= 0)
        {
            bttnBonus[2].interactable = true;
            activeSlowDown = false;
            imageSlowDown.fillAmount = 1;
            if (activeCurrent == true)
            {
                if (currentSpeed >= 3.5f)
                {
                    spawner.speed = currentSpeed - .5f;
                }
                else spawner.speed = currentSpeed;
                activeCurrent = false;
            }
        }
        else
        {
            if (currentSpeed >= 4f)
            {
                spawner.speed = currentSpeed - 1f;
            }
            else spawner.speed = 3;
            bttnBonus[2].interactable = false;
            imageSlowDown.fillAmount -= 1f / (Tslow.Timer + 2f) * Time.deltaTime;
        }
    }
}
