using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class DataBase : MonoBehaviour
{
    [SerializeField] private Save save;
    [SerializeField] private int money;
 
    [SerializeField] private int highScore;
    [SerializeField] private int healthGlobal;
    [SerializeField] private int wave;
    [SerializeField] private int shield;
    [SerializeField] private int slowDown;
    [SerializeField] private int countLevel;
    [SerializeField] private int[] lvlStar;
    [SerializeField] private bool soundOn;

    public bool SoundOn { get => soundOn; set => soundOn = value; }
    public int[] LvlStar  { get => lvlStar; set => lvlStar = value; }
    public int HighScore { get => highScore; set => highScore = value; }
    public int CountLevel { get => countLevel; set => countLevel = value; }
    public int Money { get => money; set => money = value; }
    public int HealthGlobal { get => healthGlobal; set => healthGlobal = value; }
    public int Wave { get => wave; set => wave = value; }
    public int Shield { get => shield; set => shield = value; }
    public int SlowDown { get => slowDown; set => slowDown = value; }

    public static DataBase Instance;

    private void Awake()
    {
       
        if (Instance == null) Instance = this;
        save.LG();
        HealthGlobal = 2;
       

    }

}
