﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour
{
    [SerializeField] private GameObject[] Location;
    [SerializeField] private LevelManager LevelManager;

    public float speed;
    public float speedIncrease;
    private bool firstSpawn = true;
    [SerializeField] private int spawnerTo;
    private int sc;

    private void Awake()
    {
        sc = SceneManager.GetActiveScene().buildIndex;
        if (sc == 2)
        {
            LevelManager = FindObjectOfType<LevelManager>();
            if (LevelManager.activLvl == true)
            {

                List<GameObject> lvl = new List<GameObject>(LevelManager.curLvl.Location);
                Location = new GameObject[lvl.Count];
                if (LevelManager.ListLevels.Count > 0)
                {
                    for (int i = 0; i < LevelManager.curLvl.Location.Length; i++)
                    {
                        Location[i] = lvl[i];
                    }
                }

            }
        }
        
        
    }
    
    private void Update()
    {
        speed += speedIncrease * Time.deltaTime;
        if (transform.position.y == 10 && firstSpawn)
        {
           
            gameObject.transform.position = new Vector3(0, 0, 0);
            SpawnWaveLvl();
            firstSpawn = false;
        }
        else
        {
            gameObject.transform.position = new Vector3(0, 10, 0);
        }
    }
    public void SpawnWave()
    {
       
    }
    public void SpawnWaveLvl()
    {
        
        if (sc == 2)
        {
            if (spawnerTo <= Location.Length - 1)
            {
                Instantiate(Location[spawnerTo], transform.position, Quaternion.identity);
                spawnerTo++;
            }
            else spawnerTo = 0;
        }

        else if (sc == 1)
        {
            int rand = Random.Range(0, Location.Length);
            Instantiate(Location[rand], transform.position, Quaternion.identity);
        }
            
    }
}
