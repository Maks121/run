using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class Save : MonoBehaviour
{
    public void SG()
    {
        SaveGame();
    }
    public void LG()
    {
        LoadGame();
    }
    
    private void SaveGame()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/SaveDate.dat");
        SaveDate data = new SaveDate();
        data.money = DataBase.Instance.Money;
        data.highScore = DataBase.Instance.HighScore;
        data.healthGlobal = DataBase.Instance.HealthGlobal;
        data.shield = DataBase.Instance.Shield;
        data.slowDown = DataBase.Instance.SlowDown;
        data.wave = DataBase.Instance.Wave;
        data.countLevel = DataBase.Instance.CountLevel;
        data.lvlStar = DataBase.Instance.LvlStar;
        data.soundOn = DataBase.Instance.SoundOn;
        bf.Serialize(file, data);
        file.Close();
        //Debug.Log("Game saved");   
    }
    
    private void LoadGame()
    {
        if (File.Exists(Application.persistentDataPath + "/SaveDate.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/SaveDate.dat", FileMode.Open);
            SaveDate data = (SaveDate)bf.Deserialize(file);
            file.Close();
            DataBase.Instance.Money = data.money;
            DataBase.Instance.HighScore = data.highScore;
            DataBase.Instance.HealthGlobal = data.healthGlobal;
            DataBase.Instance.Shield = data.shield;
            DataBase.Instance.SlowDown = data.slowDown;
            DataBase.Instance.Wave = data.wave;
            DataBase.Instance.CountLevel = data.countLevel;
            DataBase.Instance.LvlStar = data.lvlStar;
            DataBase.Instance.SoundOn =  data.soundOn;


            // Debug.Log("Game data loaded!");
        }
        else Debug.Log("There is no save data!!");


    }
}
[Serializable]
class SaveDate
{
    public int money;
    public int highScore;
    public int healthGlobal;
    public int wave;
    public int slowDown;
    public int shield;
    public int countLevel;
    public int[] lvlStar;
    public bool soundOn;
}
